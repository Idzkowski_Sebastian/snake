package snakeGame;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Point;

import javax.swing.JPanel;

public class RenderPanel extends JPanel{
	/**
	 * Snake Render panel
	 */
	private static final long serialVersionUID = 1L;
	private static Color green = new Color(1666073);
	
	@Override
	protected void paintComponent(Graphics g) {
		super.paintComponent(g);
		g.setColor(green);
		g.fillRect(0, 0, 300, 300);
		Snake snake = Snake.snake;
		g.setColor(Color.blue);
		for (Point point: snake.snakeParts){
			g.fillRect(point.x *Snake.SCALE, point.y*Snake.SCALE, 
					Snake.SCALE,Snake.SCALE);
		}
		g.fillRect(snake.head.x*Snake.SCALE, snake.head.y*Snake.SCALE,
				Snake.SCALE,Snake.SCALE);
		g.setColor(Color.red);
		g.fillRect(snake.cherry.x*Snake.SCALE, snake.cherry.y*Snake.SCALE,
				Snake.SCALE,Snake.SCALE);
		g.setColor(Color.white);
		g.drawString("Score: "+snake.score, snake.jframe.getWidth()-70, snake.jframe.getHeight()-70);
		g.drawString("Time: "+snake.time/10, snake.jframe.getWidth()-70, snake.jframe.getHeight()-50);
		g.drawString("Life: "+snake.life, snake.jframe.getWidth()-70, snake.jframe.getHeight()-30);
		if(snake.over){
			g.drawString("Game Over !!!",  snake.jframe.getWidth()-180, snake.jframe.getHeight()-200);
			g.drawString("Press spacebar to start again",  snake.jframe.getWidth()-230, (snake.jframe.getHeight()/2)-20);
		}
	}
}
