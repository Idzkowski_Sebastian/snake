package snakeGame;

import java.awt.Dimension;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Random;

import javax.swing.JFrame;
import javax.swing.Timer;

public class Snake implements ActionListener,KeyListener {
	
	public JFrame jframe;
	public RenderPanel renderPanel;
	public Toolkit toolkit;
	public  static Snake snake;
	public Timer timer = new Timer(20,this);

	public ArrayList<Point> snakeParts = new ArrayList<Point>();

	public static final int UP = 0, DOWN = 1, LEFT = 2, RIGHT =3, SCALE =	10; 
	public int ticks = 0, direction=DOWN, score, tailLength=10,life,time;

	public Point head,cherry;
	public Dimension dim;
	public Random random;
	public boolean over = false;
	
	
	public Snake(){
		dim = Toolkit.getDefaultToolkit().getScreenSize();
		toolkit = Toolkit.getDefaultToolkit();
		jframe = new JFrame("Snake");
		jframe.setVisible(true);
		jframe.setSize(300, 300);
		jframe.setResizable(false);
		jframe.setLocation(dim.width/2 -jframe.getWidth()/2, dim.height/2 - jframe.getHeight()/2);
		jframe.add((renderPanel)= new RenderPanel());
		jframe.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		jframe.addKeyListener(this);
		startGame();
	}
	
	public void startGame(){
		
		time = 0;
		life = 3;
		over = false;
		score =0;
		tailLength = 3;
		direction = DOWN;
		head =new Point (0,0);
		random =new Random();
		snakeParts.clear();
		cherry = new Point(random.nextInt((jframe.getHeight()/SCALE)-5), random.nextInt((jframe.getWidth()/SCALE)-5));
		for(int i=0;i<tailLength;i++){
			snakeParts.add(new Point(head.x, head.y));	
		}
		
		timer.start();
	}
	
	@Override
	public void actionPerformed(ActionEvent arg0) {
		renderPanel.repaint();
		ticks++;
		
		if(ticks% 5 ==0 && head !=null && over != true){
			time++;
			snakeParts.add(new Point(head.x, head.y));
			if(direction ==UP){
				if(head.y-1  >= 0 && noTailAt(head.x, head.y-1)){
					head = new Point(head.x, head.y-1);
				}else over = true;				
			}
			
			if(direction ==DOWN){
				if(head.y + 1 < ((jframe.getHeight()/SCALE)-2) &&noTailAt(head.x, head.y+1)){
					head = new Point(head.x, head.y+1);
				}else over = true;	
			}
			if(direction == LEFT){
				if(head.x-1 >= 0 && noTailAt(head.x-1, head.y)){
					head = new Point(head.x-1, head.y);
				} else over = true;
			}
			if(direction ==RIGHT){
				if(head.x + 1 <= ((jframe.getWidth()/SCALE)-2) && noTailAt(head.x+1, head.y)){
					head = new Point(head.x+1, head.y);
				}else over = true;
			}
			
			if(snakeParts.size()>tailLength){
				snakeParts.remove(0);
			}
		
			if(over&&life!=0){
				life--;
				over = false;
				score =0;
				tailLength = 3;
				time = 3;
				direction = DOWN;
				head =new Point (0,0);
				random =new Random();
				snakeParts.clear();
				cherry = new Point(random.nextInt((jframe.getHeight()/SCALE)-5), random.nextInt((jframe.getWidth()/SCALE)-5));
			}
					
			if(cherry != null){
				if(head.equals(cherry)){
					score += 10;
					tailLength++; 
					cherry.setLocation(random.nextInt((jframe.getHeight()/SCALE)-5), random.nextInt((jframe.getWidth()/SCALE)-5));
				}
			}
		}
		
	}
	
	private boolean noTailAt(int i, int y) {
		
		for(Point point: snakeParts){
			if(point.equals(new Point(i,y))){
				return false;
			}
		}
		return true;
	}

	public static void main(String[] args) {
		snake = new Snake();
	
	}


	@Override
	public void keyPressed(KeyEvent e) {
		// TODO Auto-generated method stub
		int i = e.getKeyCode();
		if(i==KeyEvent.VK_LEFT && direction != RIGHT){
			direction = LEFT;
		}
		if(i==KeyEvent.VK_RIGHT && direction != LEFT){
			direction = RIGHT;
		}
		if(i==KeyEvent.VK_UP && direction != DOWN){
			direction = UP;
		}
		if(i==KeyEvent.VK_DOWN && direction != UP){
			direction = DOWN;
		}
		if(i==KeyEvent.VK_SPACE && over){
			startGame();
		}
	}

	@Override
	public void keyReleased(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void keyTyped(KeyEvent e) {
		// TODO Auto-generated method stub
		
	}
	
}
